// Author: Kevin Van Kammen
// Date: 7/21/2014

// this file contains globally-visible variables and macros

// defines

// structures
typedef enum
{
    breakfast,
    lunch,
    dinner
} CarbsMeal;

typedef enum
{
    small_bite,
    lt_typical,
    typical,
    gt_typical
} CarbsAmount;

typedef struct {
  CarbsMeal meal; 
  CarbsAmount carbs; 
} CarbsData;

typedef struct {
  int glucose_level;
} GlucoseData;

typedef enum {
  small_dose,
  medium_dose,
  large_dose
} OneTimeDose;

typedef struct {
  int target_level;
  int duration_hrs;
  int duration_mins;
} TempTarget;

typedef struct {
  int reduce_pct;
  int duration_hrs;
  int duration_mins;
} ReduceDose;

typedef struct {
  OneTimeDose dose;
  TempTarget temp_target;
  ReduceDose reduce_dose;
} SafetyData;

typedef struct {
  CarbsData carbs_data;
  GlucoseData glucose_data; 
  SafetyData safety_data;
} GlobalVariables;

extern GlobalVariables global_variables;