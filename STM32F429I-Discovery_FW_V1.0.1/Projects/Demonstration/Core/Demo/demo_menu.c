/**
  ******************************************************************************
  * @file    demo_menu.c
  * @author  MCD Application Team
  * @version V1.0.1
  * @date    11-November-2013
  * @brief   Demo menu and icons
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "WM.h"
#include "Res\Meals.c"
#include "Res\Blood_Glucose.c"
#include "Res\Added_Safety.c"
#include "Res\Settings.c"
#include "time_utils.h"
#include "cpu_utils.h"
#include "global_variables.h"

/* External variables --------------------------------------------------------*/
extern __IO uint8_t alarm_now;
extern __IO uint32_t alarm_set;
extern RTC_AlarmTypeDef  RTC_AlarmStructure;
extern WM_HWIN  VIDEO_hWin, hVideoScreen;
extern WM_HWIN  IMAGE_hWin, vFrame;
extern __IO uint32_t TS_Orientation;
extern __IO uint32_t IMAGE_Enlarge;
extern __IO uint32_t VIDEO_Enlarge;


extern WM_HWIN CreateCarbs_Screen1(void); 
extern WM_HWIN CreateGlucose_Screen1(void);
extern WM_HWIN CreateSafety_Screen1(void);
extern WM_HWIN CreateSettings_Screen1(void);

/* Private typedef -----------------------------------------------------------*/
typedef struct {
  const GUI_BITMAP * pBitmap;  
  const char       * pText;
  const char       * pExplanation;
} BITMAP_ITEM;

/* Private defines -----------------------------------------------------------*/
#define WM_MSG_USB_STATUS_CHANGED      WM_USER + 0x01
#define ID_TIMER_TIME                  1

/* Private macros ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t current_module = 0xFF;

static const BITMAP_ITEM _aBitmapItem[] = {
  {&bmMeals,         "Carbohydrates"       , "Launch MPJPEG video"},
  {&bmBlood_Glucose, "Blood Glucose"       , "Browse Images"},  
  {&bmAdded_Safety,  "Added Safety"        , "Clock settings"},
  {&bmSettings,      "Settings"            , "Launch Reversi game"},
};

/*
static WM_HWIN (*_apModules[])() = 
{
  CreateCarbs_Screen1,
  CreateGlucose_Screen1,
  CreateGlucose_Screen1,
  CreateGlucose_Screen1,
};*/

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Callback routine of desktop window
  * @param  pMsg: pointer to a data structure of type WM_MESSAGE
  * @retval None
  */
//layer 1 desktop window callback
static void _cbBk(WM_MESSAGE * pMsg) {
  uint32_t NCode, Id, sel;
  //static uint32_t module_mutex = 0;
 
  switch (pMsg->MsgId) 
  {
  case WM_PAINT:
    GUI_SetBkColor(GUI_TRANSPARENT);
    GUI_Clear();
    break;
    
  case WM_NOTIFY_PARENT:
    Id    = WM_GetId(pMsg->hWinSrc);     
    NCode = pMsg->Data.v;  
    
    switch (NCode) 
    {  
    case WM_NOTIFICATION_RELEASED: 
      if (Id == '0')
      {
        sel = ICONVIEW_GetSel(pMsg->hWinSrc);
        if(sel < GUI_COUNTOF(_aBitmapItem))
        {
          current_module = sel;
          switch (sel)
          {
          case 0:
            CreateCarbs_Screen1();
            break;
          case 1:
            CreateGlucose_Screen1();
            break;
          case 2:
            CreateSafety_Screen1();
            break;
          case 3:
            CreateSettings_Screen1();
            break;
          default:
            break;
          }
        }
      }
      break;
    case WM_NOTIFICATION_CLICKED:
      //do nothing, so screen doesn't flicker
      break;
      /*
    case 0x500:
      module_mutex = 0;
      current_module = 0xFF;
      ICONVIEW_SetSel(pMsg->hWinSrc, -1);
      break;
      */
    default:
      break;
    }
    break;
  default:
    WM_DefaultProc(pMsg);
  }
}

/**
  * @brief  Demo Main menu
  * @param  None
  * @retval None
  */
void DEMO_MainMenu(void) 
{
  ICONVIEW_Handle hIcon;
  int i = 0;
  
  //Screen orientation for dual display mockup
  GUI_SetOrientationEx(GUI_MIRROR_X | GUI_MIRROR_Y, 0);
  GUI_SetOrientationEx(GUI_MIRROR_X | GUI_MIRROR_Y, 1);
  
  //layer 0 - menu background
  GUI_SetBkColor(GUI_DARKGRAY);
  GUI_Clear();
  

  //layer 1 - ICONVIEW, all dialogs
  WM_SetCallback(WM_GetDesktopWindowEx(1), _cbBk);
  
  hIcon = ICONVIEW_CreateEx(10, 0, LCD_GetXSize()-20, LCD_GetYSize(), WM_GetDesktopWindowEx(1), WM_CF_SHOW | WM_CF_HASTRANS ,0 ,'0', 100, 100); 
  
  ICONVIEW_SetFont(hIcon, &GUI_Font16B_ASCII);  
  //ICONVIEW_SetBkColor(hIcon, ICONVIEW_CI_SEL, 0x941000 | 0x80404040);  
  ICONVIEW_SetBkColor(hIcon, ICONVIEW_CI_SEL, GUI_DARKGRAY); 
  ICONVIEW_SetSpace(hIcon, GUI_COORD_Y, -10);  
  ICONVIEW_SetFrame(hIcon, GUI_COORD_Y, -10);  
  
  for (i = 0; i < GUI_COUNTOF(_aBitmapItem); i++)
  {
    ICONVIEW_AddBitmapItem(hIcon,_aBitmapItem[i].pBitmap, _aBitmapItem[i].pText);
  }  
  GUI_SelectLayer(1);  

  while (1) 
  {
    GUI_Delay(100);
  }
}

/*************************** End of file ****************************/
